package com.zuitt.s01d2;

public class DataTypesAndVariables {

    public static void main(String[] args) {
        // Variable declaration with initialization
        int firstNumber = 0;

        // Variable declaration without initialization
        int secondNumber;

        secondNumber = 23; // Initialization after declaration

        // Concatenation
        System.out.println("The value of the first number: " + firstNumber);
        System.out.println("The value of the second number: " + secondNumber);

        // Mini-activity (Primitive Data Types):

        // 1. Create a variable to contain an age value
        int age;
        age = 18;

        // 2. Create a variable to contain a human weight
        //double weight;
        //weight = 60.57;
        float weight;
        weight = 60.57f;

        // 3. Create a variable to contain a human gender
        char gender;
        gender = 'M';

        // 4. Create a variable to contain a civil status if it is single
        boolean isSingle = true;

        System.out.println("Age is: " + age);
        System.out.println("Weight is: " + weight);
        System.out.println("Gender is: " + gender);
        System.out.println("Is single: " + isSingle);

        // Non-Primitives:

        // Create a string variable for a name
        // <data_type> <identifier> = "value";

        String name = "Juan Dela Cruz";
        String concatenatedName = "Juan" + "Dela" + "Cruz";

        System.out.println("name: " + name);
        System.out.println("concatenatedName: " + concatenatedName);
    }
}
